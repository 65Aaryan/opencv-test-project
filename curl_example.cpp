#include <iostream>
#include <string>
#include <curl/curl.h>


static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

int main()
{
    CURL *curl_handle;
    CURLcode res;
    std::string readBuffer;
    curl_handle = curl_easy_init();
    if(curl_handle) {
        curl_easy_setopt(curl_handle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_easy_setopt(curl_handle, CURLOPT_URL, "https://bobblification-api-old.bobbleapp.asia/api/v2/bobble");
        curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl_handle, CURLOPT_DEFAULT_PROTOCOL, "https");
        struct curl_slist *headers = NULL;
        curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);
        curl_mime *mime;
        curl_mimepart *part;
        mime = curl_mime_init(curl_handle);
        part = curl_mime_addpart(mime);
        curl_mime_name(part, "gender");
        curl_mime_data(part, "male", CURL_ZERO_TERMINATED);
        part = curl_mime_addpart(mime);
        curl_mime_name(part, "image");
        curl_mime_filedata(part, "../resources/myImage.JPG");
        curl_easy_setopt(curl_handle, CURLOPT_MIMEPOST, mime);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &readBuffer);
        res = curl_easy_perform(curl_handle);
        curl_mime_free(mime);
    }
    curl_easy_cleanup(curl_handle);
    // save readbuffer string in a JSON file
    std::cout << readBuffer;

    return 0;
}
