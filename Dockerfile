#build an image starting with the ubuntu:18.04
FROM ubuntu:18.04

#set environment variable
ENV TZ=Asia/Kolkata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
  
#get all the dependencies:

RUN  apt update
RUN  apt install -y build-essential 
Run  apt install -y libcurl4-openssl-dev
RUN  apt install -y cmake
RUN  apt install -y git

#build OpenCV   
RUN  git clone https://github.com/opencv/opencv.git
WORKDIR /opencv
RUN git fetch -a && git fetch --all --tags
RUN git checkout tags/3.4.13 
RUN  mkdir build
WORKDIR /opencv/build
RUN  cmake .. 
RUN  make -j4 
RUN  make install 


CMD ["/bin/bash"]