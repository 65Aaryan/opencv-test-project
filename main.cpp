#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <iostream>
#include <string>

 int main()
{

    std::string pathname = "C:\\Users\\kumar\\OneDrive\\Desktop\\opencv_tuts\\myImage.JPG";

    cv::Mat img = cv::imread(pathname);
    std::cout<<"The dimension of the image are:"<<" "<<img.rows<<"x"<<img.cols<<std::endl;
    cv::Mat channelArray[3];
    cv::Range rowRange(img.rows * 0.50,img.rows * 0.70);
    cv::Range colRange(img.cols * 0.30, img.cols * 0.75);
    cv::Mat imgResize(640,480,CV_8UC3), imgCrop(img,rowRange,colRange );

    resize( img,imgResize,imgResize.size(), 0, 0, cv::INTER_AREA );
   //std::cout<<img.size()<<std::endl;
    cv::Rect roi(100,100,300,250);

    cv::Mat imgBlur;
    GaussianBlur (img, imgBlur,cv::Size(45, 45),0);

    if (img.empty())
    {
        std::cout << "Error" << std::endl;
        return -1;
    }
    cv::imshow("ImageResize", imgResize);
    cv::imshow("Aaryan Picture", img);
   // cv::split(img, channelArray);
//    cv::imshow("Blue channel",channelArray[0]);
//    cv::imshow("Green channel",channelArray[1]);
//    cv::imshow("Red channel",channelArray[2]);
      cv::imshow("Image Crop",imgCrop);
      cv::imshow("Image blur",imgBlur);
      cv::waitKey(0);

    return 0;
}


