cmake_minimum_required(VERSION 3.10)
project(opencv_test)

set(CMAKE_CXX_STANDARD 11)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

find_package(CURL REQUIRED)
message("${CURL_CONFIG} ----- ${CURL_CONSIDERED_VERSIONS} --${CURL_FOUND}")

include_directories(${CURL_INCLUDE_DIRS})
message("${CURL_LIBRARY}")

add_library(WEBP SHARED IMPORTED)
find_library(WEBPMUX NAMES libwebpmux)
find_library(WEBPDEMUX NAMES libwebpdemux)
set_target_properties(WEBP PROPERTIES
        IMPORTED_LOCATION /usr/lib/x86_64-linux-gnu/libwebp.so)
message("${WEBP}")
add_executable(opencv_test curl_example.cpp)

target_link_libraries(opencv_test PUBLIC ${WEBP} curl /usr/lib/x86_64-linux-gnu/libwebpmux.so /usr/lib/x86_64-linux-gnu/libwebpdemux.so)
